﻿using System;
using System.Collections.Generic;
using System.Text;
using Test_9.DAL.Entities.Contracts;

namespace Test_9.DAL.Entities
{
    public class Position : IEntity
    {
        public Guid Id { get; set; }
        public string PositionName { get; set; }
        public Guid DepartmentId { get; set; }
        public Department Department { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
