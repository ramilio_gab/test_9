﻿using System;

namespace Test_9.DAL.Entities.Contracts
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
