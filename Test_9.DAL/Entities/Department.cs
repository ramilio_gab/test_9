﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Test_9.DAL.Entities.Contracts;

namespace Test_9.DAL.Entities
{
    public class Department : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string DepartmentName { get; set; }
        public ICollection<User> Users { get; set; }
        public ICollection<Position> Positions { get; set; }
    }
}
