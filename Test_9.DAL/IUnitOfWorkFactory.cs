﻿using System;

namespace Test_9.DAL
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
