﻿using System;
using System.Threading.Tasks;
using Test_9.DAL.Repositories;
using Test_9.DAL.Repositories.Contracts;

namespace Test_9.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IUserRepository User { get; set; }
        public IPositionRepository Positions { get; set; }
        public IDepartmentRepository Departments { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            User = new UserRepository(context);
            Positions = new PositionRepository(context);
            Departments = new DepartmentRepository(context);
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
