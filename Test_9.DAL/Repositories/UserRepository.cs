﻿using Test_9.DAL.Entities;
using Test_9.DAL.Repositories.Contracts;

namespace Test_9.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Users;
        }
    }
}
