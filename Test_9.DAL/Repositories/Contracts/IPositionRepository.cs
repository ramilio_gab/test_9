﻿using Test_9.DAL.Entities;

namespace Test_9.DAL.Repositories.Contracts
{
    public interface IPositionRepository : IRepository<Position>
    {
    }
}
