﻿using Test_9.DAL.Entities;
using Test_9.DAL.Entities.Contracts;

namespace Test_9.DAL.Repositories.Contracts
{
    public interface IDepartmentRepository : IRepository<Department>
    {

    }
}
