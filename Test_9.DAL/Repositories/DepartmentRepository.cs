﻿using Test_9.DAL.Entities;
using Test_9.DAL.Entities.Contracts;
using Test_9.DAL.Repositories.Contracts;

namespace Test_9.DAL.Repositories
{
    public class DepartmentRepository : Repository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Departments;
        }
    }
}
