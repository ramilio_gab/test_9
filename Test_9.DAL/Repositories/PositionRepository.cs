﻿using Test_9.DAL.Entities;
using Test_9.DAL.Repositories.Contracts;

namespace Test_9.DAL.Repositories
{
    public class PositionRepository : Repository<Position>, IPositionRepository
    {
        public PositionRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Positions;
        }

    }
}
