﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Test_9.DAL.Migrations
{
    public partial class AddDefaultRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO \"AspNetRoles\" VALUES " +
                                 "('27a57bbe-ecc9-498d-8815-912265bd6d35', 'user', 'USER', '0986bc05-2542-45ac-b635-eef33ec7de20')," +
                                 "('5f0c7074-6ab8-44c0-81dd-c057b1f6676e', 'admin', 'ADMIN', '182e212d-3f44-4a7b-a60a-9f2e8f71bd8d')," +
                                 "('3e17aedb-74ec-4bf7-b3a6-cdff969a40b0', 'сhiefDepartments', 'ChiefDepartments', '42aff74d-04ee-4ac0-a241-45d357e67ad5'); ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
