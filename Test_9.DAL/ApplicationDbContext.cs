﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Test_9.DAL.Entities;
using Test_9.DAL.Entities.Contracts;
using Test_9.DAL.EntitiesConfiguration.Contracts;

namespace Test_9.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, Guid>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;


        public DbSet<Position> Positions { get; set; }
        public DbSet<Department> Departments { get; set; }

        public ApplicationDbContext(DbContextOptions options, IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity(_entityConfigurationsContainer.UserConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.PositionConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.DepartmentConfiguration.ProvideConfigurationAction());
        }
    }
}
