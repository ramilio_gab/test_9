﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Test_9.DAL.Entities;
using Test_9.DAL.Entities.Contracts;

namespace Test_9.DAL.EntitiesConfiguration
{
    public class DepartmentConfiguration : BaseEntityConfiguration<Department>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Department> builder)
        {
            builder
                .Property(b => b.DepartmentName)
                .HasMaxLength(500)
                .IsRequired();

            builder
                .HasIndex(b => b.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Department> builder)
        {
            builder
                .HasMany(b => b.Positions)
                .WithOne(b => b.Department)
                .HasForeignKey(b => b.DepartmentId);

            builder
                .HasMany(e => e.Users)
                .WithOne(e => e.Department)
                .HasForeignKey(e => e.DepartmentId);
        }
    }
}
