﻿using Test_9.DAL.Entities;
using Test_9.DAL.EntitiesConfiguration.Contracts;

namespace Test_9.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<User> UserConfiguration { get; set; }
        public IEntityConfiguration<Position> PositionConfiguration { get; }
        public IEntityConfiguration<Department> DepartmentConfiguration { get; }


        public EntityConfigurationsContainer()
        {
            UserConfiguration = new UserConfiguration();
            PositionConfiguration = new PositionConfiguration();
            DepartmentConfiguration = new DepartmentConfiguration();
        }
    }
}
