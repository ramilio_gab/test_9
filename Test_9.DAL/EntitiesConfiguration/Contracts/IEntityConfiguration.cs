﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Test_9.DAL.Entities.Contracts;

namespace Test_9.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
