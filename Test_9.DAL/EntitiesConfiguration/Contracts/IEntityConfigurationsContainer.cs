﻿using System;
using Test_9.DAL.Entities;
using Test_9.DAL.Entities.Contracts;

namespace Test_9.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<User> UserConfiguration { get; set; }
        IEntityConfiguration<Position> PositionConfiguration { get; }
        IEntityConfiguration<Department> DepartmentConfiguration { get; }
    }
}
