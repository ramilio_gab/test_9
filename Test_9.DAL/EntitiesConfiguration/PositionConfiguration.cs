﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Test_9.DAL.Entities;

namespace Test_9.DAL.EntitiesConfiguration
{
    public class PositionConfiguration : BaseEntityConfiguration<Position>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Position> builder)
        {
            builder
                .Property(p => p.PositionName)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .HasIndex(p => p.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Position> builder)
        {
            builder
                .HasOne(p => p.Department)
                .WithMany(p => p.Positions)
                .HasForeignKey(p => p.DepartmentId);

            builder
                .HasMany(p => p.Users)
                .WithOne(p => p.Position)
                .HasForeignKey(p => p.PositionId);
        }
    }
}
