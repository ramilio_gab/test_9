﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Test_9.DAL.Entities;

namespace Test_9.DAL.EntitiesConfiguration
{
    public class UserConfiguration : BaseEntityConfiguration<User>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<User> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(b => b.Surname)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .Property(b => b.Patronymic)
                .HasMaxLength(100)
                .IsRequired();

            builder
                .HasIndex(u => u.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<User> builder)
        {

        }
    }
}
