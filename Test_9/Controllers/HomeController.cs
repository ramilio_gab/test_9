﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Test_9.DAL.Entities;
using Test_9.Models;
using Test_9.Models.AccountModels;
using Test_9.Services.Account.Contracts;

namespace Test_9.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAccountService _accountService;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public HomeController(ILogger<HomeController> logger, IAccountService accountService, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _logger = logger;
            _accountService = accountService;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Index()
        {
            var isBool = _accountService.CheckAdin();
            if (isBool == false)
            {
                var model=_accountService.CreateAdmin();

                return RedirectToAction("Register", "Home", model);
            }
            return View();
        }

        public IActionResult Admin()
        {
            return View();
        }


        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<IActionResult> Register(RegisterModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    User user = await _accountService.GetUserToRegisterAsync(model);
                    
                    var result = await _userManager.CreateAsync(user, model.Password);

                    var userAdmin = _userManager.Users.FirstOrDefault(u => u.Name == "admin");

                    await _userManager.AddToRoleAsync(userAdmin, "admin");

                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, false);

                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }

                return View(model);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
