﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Test_9.Models.DepartmentModels;
using Test_9.Services.Departments;

namespace Test_9.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly IDepartmentService _departmentService;

        public DepartmentController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = _departmentService.GetDepartments();
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Create()
        {
            DepartmentCreateModel model = _departmentService.GetDeparmentCreateModel();
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public IActionResult Create(DepartmentCreateModel model)
        {
            if (ModelState.IsValid)
            {
                _departmentService.CreateDepartment(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        [Authorize]
        public IActionResult Edit(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "Department id cannot be NULL";
                return View("BadRequest");
            }

            var departmentEditMpdel = _departmentService.GetDepartmentById(id.Value);

            return View(departmentEditMpdel);
        }

        [HttpPost]
        [Authorize]
        public IActionResult Edit(DepartmentEditModel model)
        {
            try
            {
                _departmentService.EditDepartment(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (!id.HasValue)
            {
                ViewBag.BadRequestMessage = "News id cannot be NULL";
                return View("BadRequest");
            }
            bool isDeleted = await _departmentService.DeleteDepartmentAsync(id.Value);
            return RedirectToAction("Index");
        }
    }
}
