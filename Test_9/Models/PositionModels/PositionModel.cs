﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Test_9.DAL.Entities;

namespace Test_9.Models.PositionModels
{
    public class PositionModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Должность")]
        public string PositionName { get; set; }

        [Required]
        [Display(Name = "Id подразделения")]
        public Guid DepartmentId { get; set; }

        [Required]
        [Display(Name = "Подразделение")]
        public string Department { get; set; }

        public List<Department> Departments { get; set; }
    }
}
