﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Test_9.Models.PositionModels
{
    public class PositionEditModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "необходимо указать Должность")]
        [Display(Name = "Должность")]
        [RegularExpression(@"^[ \()\ \-\ а-яА-Я]+$", ErrorMessage = "Используйте только буквы \"кириллицы\"!")]
        [MaxLength(50, ErrorMessage = "Максимальная длина строки 50 символов")]
        public string PositionName { get; set; }

        [Display(Name = "Отдел")]
        public SelectList DepartmentSelectList { get; set; }

        [Required(ErrorMessage = "необходимо указать Подразделение")]
        [Display(Name = "Подразделение")]
        public Guid DepartmentId { get; set; }
    }
}
