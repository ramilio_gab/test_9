﻿using System.ComponentModel.DataAnnotations;

namespace Test_9.Models.AccountModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Поле \"Email\" должно быть заполненно")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле \"Пароль\" должно быть заполненно")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить?")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
