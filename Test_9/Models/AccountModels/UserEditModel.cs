﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Test_9.Models.AccountModels
{
    public class UserEditModel
    {
        public Guid Id { get; set; }


        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Поле \"Фамилия\" должно быть заполненно")]
        [Display(Name = "Фамилия")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        [RegularExpression(@"^[а-яА-Я]+$", ErrorMessage = "Используйте только буквы!")]
        public string Surname { get; set; }


        [Required(ErrorMessage = "Поле \"Имя\" должно быть заполненно")]
        [Display(Name = "Имя")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        [RegularExpression(@"^[а-яА-Я]+$", ErrorMessage = "Используйте только буквы!")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Поле \"Псевдоним (Nickname)\" должно быть заполненно")]
        [Display(Name = "Псевдоним (Nickname)")]
        [MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        public string NickName { get; set; }


        [Required(ErrorMessage = "Поле \"MobilePhone\" должно быть заполненно")]
        [Display(Name = "MobilePhone")]
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid phone")]
        public string MobilePhone { get; set; }
    }
}
