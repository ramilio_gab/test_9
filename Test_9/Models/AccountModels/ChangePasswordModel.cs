﻿using System.ComponentModel.DataAnnotations;

namespace Test_9.Models.AccountModels
{
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Введите страрый пароль")]
        public string Password { get; set; }


        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Введите новый пароль")]
        public string NewPassword { get; set; }


        [Required]
        [Compare("NewPassword", ErrorMessage = "Пароли не совпадают")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердить новый пароль")]
        public string ConfirmNewPassword { get; set; }
    }
}
