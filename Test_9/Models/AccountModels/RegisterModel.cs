﻿using System.ComponentModel.DataAnnotations;

namespace Test_9.Models.AccountModels
{
    public class RegisterModel
    {
        //[Required(ErrorMessage = "Поле \"Email\" должно быть заполненно")]
        //[Display(Name = "Email")]
        //[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }


        //[Required(ErrorMessage = "Поле \"Фамилия\" должно быть заполненно")]
        //[Display(Name = "Фамилия")]
        //[MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        //[RegularExpression(@"^[а-яА-Я]+$", ErrorMessage = "Используйте только буквы!")]
        public string Surname { get; set; }


        //[Required(ErrorMessage = "Поле \"Имя\" должно быть заполненно")]
        //[Display(Name = "Имя")]
        //[MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        //[RegularExpression(@"^[а-яА-Я]+$", ErrorMessage = "Используйте только буквы!")]
        public string Name { get; set; }


        //[Required(ErrorMessage = "Поле \"Отчество\" должно быть заполненно")]
        //[Display(Name = "Имя")]
        //[MaxLength(100, ErrorMessage = "Максимальная длина 100 символов!")]
        //[RegularExpression(@"^[а-яА-Я]+$", ErrorMessage = "Используйте только буквы!")]
        public string Patronymic { get; set; }


        //[Required(ErrorMessage = "Поле \"Пароль\" должно быть заполненно")]
        //[DataType(DataType.Password)]
        //[Display(Name = "Пароль")]
        public string Password { get; set; }


        //[Required(ErrorMessage = "Поле \"Подтвердить пароль\" должно быть заполненно")]
        //[Compare("Password", ErrorMessage = "Пароли не совпадают")]
        //[DataType(DataType.Password)]
        //[Display(Name = "Подтвердить пароль")]
        public string PasswordConfirm { get; set; }
    }
}
