﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Test_9.Models.DepartmentModels
{
    public class DepartmentModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Название отдела")]
        public string DepartmentName { get; set; }
    }
}
