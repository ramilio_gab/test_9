﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Test_9.Models.DepartmentModels
{
    public class DepartmentCreateModel
    {
        [Required(ErrorMessage = "необходимо указать наименование отдела")]
        [Display(Name = "Название отдела")]
        [RegularExpression(@"^[ а-яА-Я]+$", ErrorMessage = "Используйте только буквы \"кириллицы\"!")]
        [MaxLength(35, ErrorMessage = "Максимальная длина строки 35 символов")]
        public string DepartmentName { get; set; }
    }
}
