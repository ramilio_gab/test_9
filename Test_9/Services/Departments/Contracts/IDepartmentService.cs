﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Test_9.Models.DepartmentModels;

namespace Test_9.Services.Departments
{
    public interface IDepartmentService
    {
        Task<bool> DeleteDepartmentAsync(Guid id);
        void EditDepartment(DepartmentEditModel model);
        DepartmentEditModel GetDepartmentById(Guid id);
        void CreateDepartment(DepartmentCreateModel model);
        DepartmentCreateModel GetDeparmentCreateModel();
        List<DepartmentModel> GetDepartments();
    }
}
