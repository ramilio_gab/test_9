﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using Test_9.DAL;
using Test_9.DAL.Entities;
using Test_9.Models.DepartmentModels;
using Test_9.Services.Departments;

namespace Test_9.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public DepartmentService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<DepartmentModel> GetDepartments()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var department = unitOfWork.Departments.GetAll().ToList();
                return Mapper.Map<List<DepartmentModel>>(department);
            }
        }

        public DepartmentCreateModel GetDeparmentCreateModel()
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                return new DepartmentCreateModel() { };
            }
        }

        public void CreateDepartment(DepartmentCreateModel model)
        {
            model.DepartmentName = Regex.Replace(model.DepartmentName, @"\s+", " ").Trim();
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var department = Mapper.Map<Department>(model);
                
                unitOfWork.Departments.Create(department);
            }
        }

        public DepartmentEditModel GetDepartmentById(Guid id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var department = unitOfWork.Departments.GetById(id);
                var model = Mapper.Map<DepartmentEditModel>(department);
               
                return model;
            }
        }

        public void EditDepartment(DepartmentEditModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var department = Mapper.Map<Department>(model);
                unitOfWork.Departments.Update(department);
            }
        }

        public async Task<bool> DeleteDepartmentAsync(Guid id)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var department = await uow.Departments.GetByIdAsync(id);
                try
                {
                    uow.Departments.Remove(department);
                    await uow.CompleteAsync();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
    }
}
