﻿using System.Threading.Tasks;
using Test_9.DAL.Entities;
using Test_9.Models.AccountModels;

namespace Test_9.Services.Account.Contracts
{
    public interface IAccountService
    {
        RegisterModel CreateAdmin();
        bool CheckAdin();
        RegisterModel GetRegisterModel();
        Task<User> GetUserToRegisterAsync(RegisterModel model);
        UserModel GetUserModel(User user);
        UserEditModel GetUserEditModel(User user);
        Task<User> EditUser(UserEditModel model, User user);
    }
}
