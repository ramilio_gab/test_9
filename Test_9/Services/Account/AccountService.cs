﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Test_9.DAL;
using Test_9.DAL.Entities;
using Test_9.Models.AccountModels;
using Test_9.Services.Account.Contracts;

namespace Test_9.Services.Account
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;


        public AccountService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public AccountService(UserManager<User> userManager, SignInManager<User> signInManager, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public RegisterModel CreateAdmin()
        {
            RegisterModel admin = new RegisterModel()
            {
                Email = "admin@admin",
                Surname ="admin",
                Name = "admin",
                Patronymic = "admin",
                Password = "admin",
                PasswordConfirm = "admin"
            };
            
            return admin;
        }

        public bool CheckAdin()
        {
            var isCreate = true;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var users = unitOfWork.User.GetAll().ToList().Count;
                if (users == 0)
                {
                    isCreate = false;
                }

                return isCreate;
            }
        }


        public async Task<User> EditUser(UserEditModel model, User user)
        {
            user = Mapper.Map(model, user);
            user.UserName = model.Email;
            
            return user;
        }

        public RegisterModel GetRegisterModel()
        {
            return new RegisterModel { };
        }

        public UserEditModel GetUserEditModel(User user)
        {
            UserEditModel userEditModel = Mapper.Map<UserEditModel>(user);

            return userEditModel;
        }

        public UserModel GetUserModel(User user)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                UserModel userModel = Mapper.Map<UserModel>(user);

                return userModel;
            }
        }

        public async Task<User> GetUserToRegisterAsync(RegisterModel model)
        {
            User user = Mapper.Map<User>(model);
            user.UserName = model.Email;

            return user;
        }
    }
}
