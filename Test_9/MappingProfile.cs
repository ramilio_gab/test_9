﻿using AutoMapper;
using Test_9.DAL.Entities;
using Test_9.Models.AccountModels;
using Test_9.Models.DepartmentModels;

namespace Test_9
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateModelMap<User, RegisterModel>();
            CreateModelMap<User, UserEditModel>();
            CreateModelMap<User, UserModel>();
            CreateModelMap<Department, DepartmentModel>();
            CreateModelMap<Department, DepartmentCreateModel>();
            CreateModelMap<Department, DepartmentEditModel>();
            //CreateModelMap<Restaurant, RestaurantModel>();
            //CreateModelMap<Restaurant, RestaurantCreateModel>();
        }

        private void CreateModelMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }
    }
}
